# UE4 Fill Material Example

There are 2 methods presented here, one with a timeline and one without. The one without, doesn't handle "finishing" an fill request.
In the Demo scene, any object entering the area with the sphere collision will start activation.

M_Fill - the material which contains the math for the hard gradient.
BP_Pad - the pad that will show the effect if an object that can generate overlap events is within the trigger area

### TODO:
- Clamp lerps T property between 0 and 1 for sanity in material